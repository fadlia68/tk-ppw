from django.urls import include, path
from . import views

app_name = 'happy'

urlpatterns = [
    path('', views.home, name='home'),
    path('watch/', views.watch, name='watch'),
    path('games/', views.games, name='games'),
    path('music/', views.music, name='music'),
    path('search/', views.searchKeyword, name='searchKeyword'),
    path('detail/<str:detail_id>/', views.detail, name='detail'),
    path('netflix/', views.netflix, name='netflix'),
    path('deleteNetflix/<int:delete_id>/', views.deleteNetflix, name='deleteNetflix'),
    path('anime/', views.anime, name='anime'),
    path('deleteAnime/<int:delete_id>/', views.deleteAnime, name='deleteAnime'),
    path('kdrama/', views.kdrama, name='kdrama'),
    path('deleteKdrama/<int:delete_id>/', views.deleteKdrama, name='deleteKdrama'),
    path('romantic/', views.romantic, name='romantic'),
    path('deleteRomantic/<int:delete_id>/', views.deleteRomantic, name='deleteRomantic'),
    path('tiktok/', views.tiktok, name='tiktok'),
    path('deleteTiktok/<int:delete_id>/', views.deleteTiktok, name='deleteTiktok'),
    path('podcast/', views.podcast, name='podcast'),
    path('deletePodcast/<int:delete_id>/', views.deletePodcast, name='deletePodcast'),
    path('kpop/', views.kpop, name='kpop'),
    path('deleteKpop/<int:delete_id>/', views.deleteKpop, name='deleteKpop'),
    path('games1/', views.games1, name='games1'),
    path('deleteGames1/<int:delete_id>/', views.deleteGames1, name='deleteGames1'),
    path('games2/', views.games2, name='games2'),
    path('deleteGames2/<int:delete_id>/', views.deleteGames2, name='deleteGames2'),
    # dilanjutkan ...
]
