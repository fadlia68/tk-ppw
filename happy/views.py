from django.shortcuts import render, redirect 
from .models import Article, Comment, CommentKdrama, CommentPodcast, CommentAnime, CommentKpop, CommentRomantic, CommentGames1, CommentGames2, CommentTiktok
from . import forms

# Create your views here.
def home(request):
    return render(request, 'home.html')

def watch(request):
    return render(request, 'watch.html')

def music(request):
    return render(request, 'music.html')

def games(request):
    return render(request, 'games.html')

# ======================================

def searchKeyword(request):
    word = request.GET.get('search_keyword')
    object_list = Article.objects.filter(keyword__icontains=word)
    return render(request, 'searchView.html', {'list':object_list}) 

def detail(request, detail_id):
    blog_detail = Article.objects.get(name = detail_id)
    if blog_detail.name == 'netflix':
        return redirect('/happy/netflix')
    elif blog_detail.name == 'kdrama':
        return redirect('/happy/kdrama')
    elif blog_detail.name == 'romantic':
        return redirect('/happy/romantic')
    elif blog_detail.name == 'anime':
        return redirect('/happy/anime')
    elif blog_detail.name == 'tiktok':
        return redirect('/happy/tiktok')
    elif blog_detail.name == 'podcast':
        return redirect('/happy/podcast')
    elif blog_detail.name == 'kpop':
        return redirect('/happy/kpop')
    elif blog_detail.name == 'games1':
        return redirect('/happy/games1')
    elif blog_detail.name == 'games2':
        return redirect('/happy/games2')
    else:
        return redirect('')

# ======================================

def netflix(request):
    comment_form = forms.CommentForm()
    if request.method ==  'POST':
        comment_form_input = forms.CommentForm(request.POST)
        if comment_form_input.is_valid():
            data = comment_form_input.cleaned_data
            comment_input = Comment()
            comment_input.author = data['author'] 
            comment_input.email = data['email'] 
            comment_input.content = data['content']
            comment_input.save()
            #current_data = Comment.objects.all()

            return redirect('/happy/netflix')   
        else:
            current_data = Comment.objects.all()
            return render(request, 'netflix.html', {'form':comment_form, 'status':'failed', 'data':current_data})      
    else:
        current_data = Comment.objects.all()
        return render(request, 'netflix.html', {'form':comment_form, 'data':current_data})      

def deleteNetflix(request, delete_id):
    try:
        deleted_subject = Comment.objects.get(pk = delete_id)
        deleted_subject.delete()
        return redirect('/happy/netflix')
    except:
        return redirect('/happy/netflix')

def anime(request):
    comment_form = forms.CommentForm()
    if request.method ==  'POST':
        comment_form_input = forms.CommentForm(request.POST)
        if comment_form_input.is_valid():
            data = comment_form_input.cleaned_data
            comment_input = CommentAnime()
            comment_input.author = data['author'] 
            comment_input.email = data['email'] 
            comment_input.content = data['content']
            comment_input.save()
            #current_data = Comment.objects.all()

            return redirect('/happy/anime')   
        else:
            current_data = CommentAnime.objects.all()
            return render(request, 'anime.html', {'form':comment_form, 'status':'failed', 'data':current_data})      
    else:
        current_data = CommentAnime.objects.all()
        return render(request, 'anime.html', {'form':comment_form, 'data':current_data})

def deleteAnime(request, delete_id):
    try:
        deleted_subject = CommentAnime.objects.get(pk = delete_id)
        deleted_subject.delete()
        return redirect('/happy/anime')
    except:
        return redirect('/happy/anime')


def kdrama(request):
    comment_form = forms.CommentForm()
    if request.method ==  'POST':
        comment_form_input = forms.CommentForm(request.POST)
        if comment_form_input.is_valid():
            data = comment_form_input.cleaned_data
            comment_input = CommentKdrama()
            comment_input.author = data['author'] 
            comment_input.email = data['email'] 
            comment_input.content = data['content']
            comment_input.save()
            #current_data = Comment.objects.all()

            return redirect('/happy/kdrama')   
        else:
            current_data = CommentKdrama.objects.all()
            return render(request, 'kdrama.html', {'form':comment_form, 'status':'failed', 'data':current_data})      
    else:
        current_data = CommentKdrama.objects.all()
        return render(request, 'kdrama.html', {'form':comment_form, 'data':current_data})      

def deleteKdrama(request, delete_id):
    try:
        deleted_subject = CommentKdrama.objects.get(pk = delete_id)
        deleted_subject.delete()
        return redirect('/happy/kdrama')
    except:
        return redirect('/happy/kdrama')

def podcast(request):
    comment_form = forms.CommentForm()
    if request.method ==  'POST':
        comment_form_input = forms.CommentForm(request.POST)
        if comment_form_input.is_valid():
            data = comment_form_input.cleaned_data
            comment_input = CommentPodcast()
            comment_input.author = data['author'] 
            comment_input.email = data['email'] 
            comment_input.content = data['content']
            comment_input.save()
            #current_data = Comment.objects.all()

            return redirect('/happy/podcast')   
        else:
            current_data = CommentPodcast.objects.all()
            return render(request, 'podcast.html', {'form':comment_form, 'status':'failed', 'data':current_data})      
    else:
        current_data = CommentPodcast.objects.all()
        return render(request, 'podcast.html', {'form':comment_form, 'data':current_data})      

def deletePodcast(request, delete_id):
    try:
        deleted_subject = CommentPodcast.objects.get(pk = delete_id)
        deleted_subject.delete()
        return redirect('/happy/podcast')
    except:
        return redirect('/happy/podcast')

def kpop(request):
    comment_form = forms.CommentForm()
    if request.method ==  'POST':
        comment_form_input = forms.CommentForm(request.POST)
        if comment_form_input.is_valid():
            data = comment_form_input.cleaned_data
            comment_input = CommentKpop()
            comment_input.author = data['author'] 
            comment_input.email = data['email'] 
            comment_input.content = data['content']
            comment_input.save()
            #current_data = Comment.objects.all()

            return redirect('/happy/kpop')   
        else:
            current_data = CommentKpop.objects.all()
            return render(request, 'kpop.html', {'form':comment_form, 'status':'failed', 'data':current_data})      
    else:
        current_data = CommentKpop.objects.all()
        return render(request, 'kpop.html', {'form':comment_form, 'data':current_data})      

def deleteKpop(request, delete_id):
    try:
        deleted_subject = CommentKpop.objects.get(pk = delete_id)
        deleted_subject.delete()
        return redirect('/happy/kpop')
    except:
        return redirect('/happy/kpop')

def games1(request):
    comment_form = forms.CommentForm()
    if request.method ==  'POST':
        comment_form_input = forms.CommentForm(request.POST)
        if comment_form_input.is_valid():
            data = comment_form_input.cleaned_data
            comment_input = CommentGames1()
            comment_input.author = data['author'] 
            comment_input.email = data['email'] 
            comment_input.content = data['content']
            comment_input.save()
            #current_data = Comment.objects.all()

            return redirect('/happy/games1')   
        else:
            current_data = CommentGames1.objects.all()
            return render(request, 'games1.html', {'form':comment_form, 'status':'failed', 'data':current_data})      
    else:
        current_data = CommentGames1.objects.all()
        return render(request, 'games1.html', {'form':comment_form, 'data':current_data})      

def deleteGames1(request, delete_id):
    try:
        deleted_subject = CommentGames1.objects.get(pk = delete_id)
        deleted_subject.delete()
        return redirect('/happy/games1')
    except:
        return redirect('/happy/games1')

def games2(request):
    comment_form = forms.CommentForm()
    if request.method ==  'POST':
        comment_form_input = forms.CommentForm(request.POST)
        if comment_form_input.is_valid():
            data = comment_form_input.cleaned_data
            comment_input = CommentGames2()
            comment_input.author = data['author'] 
            comment_input.email = data['email'] 
            comment_input.content = data['content']
            comment_input.save()
            #current_data = Comment.objects.all()

            return redirect('/happy/games2')   
        else:
            current_data = CommentGames2.objects.all()
            return render(request, 'games2.html', {'form':comment_form, 'status':'failed', 'data':current_data})      
    else:
        current_data = CommentGames2.objects.all()
        return render(request, 'games2.html', {'form':comment_form, 'data':current_data})      

def deleteGames2(request, delete_id):
    try:
        deleted_subject = CommentGames2.objects.get(pk = delete_id)
        deleted_subject.delete()
        return redirect('/happy/games2')
    except:
        return redirect('/happy/games2')

def romantic(request):
    comment_form = forms.CommentForm()
    if request.method ==  'POST':
        comment_form_input = forms.CommentForm(request.POST)
        if comment_form_input.is_valid():
            data = comment_form_input.cleaned_data
            comment_input = CommentRomantic()
            comment_input.author = data['author'] 
            comment_input.email = data['email'] 
            comment_input.content = data['content']
            comment_input.save()
            #current_data = Comment.objects.all()

            return redirect('/happy/romantic')   
        else:
            current_data = CommentRomantic.objects.all()
            return render(request, 'romantic.html', {'form':comment_form, 'status':'failed', 'data':current_data})      
    else:
        current_data = CommentRomantic.objects.all()
        return render(request, 'romantic.html', {'form':comment_form, 'data':current_data})      

def deleteRomantic(request, delete_id):
    try:
        deleted_subject = CommentRomantic.objects.get(pk = delete_id)
        deleted_subject.delete()
        return redirect('/happy/romantic')
    except:
        return redirect('/happy/romantic')

def tiktok(request):
    comment_form = forms.CommentForm()
    if request.method ==  'POST':
        comment_form_input = forms.CommentForm(request.POST)
        if comment_form_input.is_valid():
            data = comment_form_input.cleaned_data
            comment_input = CommentTiktok()
            comment_input.author = data['author'] 
            comment_input.email = data['email'] 
            comment_input.content = data['content']
            comment_input.save()
            #current_data = Comment.objects.all()

            return redirect('/happy/tiktok')   
        else:
            current_data = CommentTiktok.objects.all()
            return render(request, 'tiktok.html', {'form':comment_form, 'status':'failed', 'data':current_data})      
    else:
        current_data = CommentTiktok.objects.all()
        return render(request, 'tiktok.html', {'form':comment_form, 'data':current_data})      

def deleteTiktok(request, delete_id):
    try:
        deleted_subject = CommentTiktok.objects.get(pk = delete_id)
        deleted_subject.delete()
        return redirect('/happy/tiktok')
    except:
        return redirect('/happy/tiktok')

