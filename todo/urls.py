from django.urls import include, path
from . import views

app_name = 'todo'

urlpatterns = [
    path('', views.index, name='index'),
    path('addTask/',  views.addTask, name='addTask'),
    path('personalTask/', views.personalTask, name='personalTask'),
    path('groupTask/', views.groupTask, name='groupTask'),
    path('deleteTask/<int:task_id>', views.deleteTask, name='deleteTask'),
]
