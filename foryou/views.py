from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import Pesan
import random

lst = ["Alligator","Anteater","Armadillo","Auroch","Axolotl","Badger","Bat","Beaver","Buffalo","Camel","Capybara","Chameleon","Cheetah","Chinchilla","Chipmunk","Chupacabra","Cormorant","Coyote","Crow","Dingo","Dinosaur","Dolphin","Duck","Elephant","Ferret","Fox","Frog","Giraffe","Gopher","Grizzly","Hedgehog","Hippo","Hyena","Ibex","Ifrit","Iguana","Jackal","Kangaroo","Koala","Kraken","Lemur","Leopard","Liger","Llama","Manatee","Mink","Monkey","Moose","Narwhal","Orangutan","Otter","Panda","Penguin","Platypus","Pumpkin","Python","Quagga","Rabbit","Raccoon","Rhino","Sheep","Shrew","Skunk","Squirrel","Tiger","Turtle","Walrus","Wolf","Wolverine","Wombat"]

def index(request):
    return render(request, 'main/index.html')

def read(request):
    message = Pesan.objects.all()
    response = {'messages' : message}
    return render(request, 'main/read.html', response)

def send(request):
    response = {'form' : Input_Form, 'success' : False}
    return render(request, 'main/send.html', response)

def save(request):
    form = Input_Form(request.POST)
    if (request.method == 'POST' and form.is_valid()):
        nama = form.cleaned_data['name']
        pesan = form.cleaned_data['message']
        if (nama == ''):
            hewan = random.choice(lst)
            lst.remove(hewan)
            new = Pesan(name="Anonymous " + hewan, message=pesan, awal=hewan[0])
        else:
            new = Pesan(name=nama, message=pesan, awal=nama[0].upper())
        new.save()
        response = {'form' : Input_Form, 'success' : True}
        return render(request, 'main/send.html', response)
    return HttpResponseRedirect('/foryou/send')