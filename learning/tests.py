from django.test import TestCase, Client
from .models import Category, Subject
from django.urls import reverse
from .forms import SubjectForm


# Create your tests here.
class Testing(TestCase):
    def setUp(self):
        self.pro = Category.objects.create(name="programming")
        Category.objects.create(name="math")
        Category.objects.create(name="science")
        Category.objects.create(name="language")
        Category.objects.create(name="business")
        Category.objects.create(name="others")
        self.sub1 = Subject.objects.create(name="sub1")
        Subject.objects.create(name="sub2")
        Subject.objects.create(name="sub3")

    def test_url_learning_exist(self):
        response = Client().get('/learning/')
        self.assertEqual(response.status_code, 200)

    def test_url_addSource_exist(self):
        response = Client().get('/learning/addSource/')
        self.assertEqual(response.status_code, 200)

    def test_matching_addSource_view(self):
        response = Client().get('/learning/addSource/')
        self.assertTemplateUsed(response, 'main/addSource.html')

    def test_nama_model_category(self):
        self.assertEqual(str(self.pro), 'programming')

    def test_nama_model_subject(self):
        self.assertEqual(str(self.sub1), 'sub1')

    def test_apakah_model_subject_ada(self):
        hitung_berapa = Subject.objects.all().count()
        self.assertEquals(hitung_berapa, 3)

    def test_valid_add_source(self):
        response = Client().post('/learning/addSource/', data={'name':'namanya'})
        self.assertEqual(response.status_code, 200)

    def test_url_programming_exist(self):
        response = Client().get('/learning/programming/')
        self.assertEqual(response.status_code, 200)

    def test_matching_programming_view(self):
        response = Client().get('/learning/programming/')
        self.assertTemplateUsed(response, 'main/more.html')
    
    def test_url_math_exist(self):
        response = Client().get('/learning/math/')
        self.assertEqual(response.status_code, 200)

    def test_matching_math_view(self):
        response = Client().get('/learning/math/')
        self.assertTemplateUsed(response, 'main/more.html')

    def test_url_science_exist(self):
        response = Client().get('/learning/science/')
        self.assertEqual(response.status_code, 200)

    def test_matching_science_view(self):
        response = Client().get('/learning/science/')
        self.assertTemplateUsed(response, 'main/more.html')

    def test_url_language_exist(self):
        response = Client().get('/learning/language/')
        self.assertEqual(response.status_code, 200)

    def test_matching_language_view(self):
        response = Client().get('/learning/language/')
        self.assertTemplateUsed(response, 'main/more.html')

    def test_url_business_exist(self):
        response = Client().get('/learning/business/')
        self.assertEqual(response.status_code, 200)

    def test_matching_business_view(self):
        response = Client().get('/learning/business/')
        self.assertTemplateUsed(response, 'main/more.html')

    def test_url_others_exist(self):
        response = Client().get('/learning/others/')
        self.assertEqual(response.status_code, 200)

    def test_matching_others_view(self):
        response = Client().get('/learning/others/')
        self.assertTemplateUsed(response, 'main/more.html')
    
    def test_content_in_learning_space(self):
        response = self.client.get('/learning/addSource/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Add Another Source", html_response)
        self.assertIn("Type a description", html_response)

    def test_url_all_exist(self):
        response = Client().get('/learning/allSource/')
        self.assertEqual(response.status_code, 200)

    def test_content_in_all_page(self):
        response = self.client.get('/learning/allSource/')
        html_response = response.content.decode('utf-8')
        self.assertIn("All Sources", html_response)

    def test_matching_allSource_view(self):
        response = Client().get('/learning/allSource/')
        self.assertTemplateUsed(response, 'main/more.html')