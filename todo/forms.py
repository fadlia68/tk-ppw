from django.forms import ModelForm
from .models import Task

class TaskForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Task
        fields = [
            "title",
            "description",
            "category",
        ]