from django.urls import include, path
from .views import index, read, send, save

urlpatterns = [
    path('', index, name='index'),
    path('read/', read, name='read'),
    path('send/', send, name='send'),
    path('send/save', save, name='save')
]
