from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from .forms import RegisForm
from django.utils.safestring import mark_safe

# Create your views here.

def index(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)

        else :
            messages.error(request,'Username or password not correct')

    return render(request, 'index.html')

def register(request):
    if request.method == "POST":
        form = RegisForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Congratulations, your account has been successfully created! Please Kindly Login on the Login Button')
        else :
            messages.error(request, mark_safe("ERROR: Something went wrong. User Registration failed<br/>Requirements:<br/>1. Username required 150 characters or fewer. Letters, digits, and @/./+/-/_ only.<br/>2. Enter the same password as before<br/>3. Your password must contain at least 8 character<br/>4. Your password can't be a commonly used password, entirely numeric, and similiarlu to username"))

    else :
        form = RegisForm()
    return render(request, 'register.html', {'form' : form})

def view_logout(request):
    logout(request)
    return redirect('/')
