from django.forms import ModelForm
from .models import *
from django.forms.widgets import CheckboxSelectMultiple

class SubjectForm(ModelForm):
    class Meta:
        model = Subject
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        
        super(SubjectForm, self).__init__(*args, **kwargs)
        
        self.fields["category"].widget = CheckboxSelectMultiple()
        self.fields["category"].queryset = Category.objects.all()