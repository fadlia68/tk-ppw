from django.contrib import admin
from .models import Comment, Article

# Register your models here.
admin.register(Comment, Article)(admin.ModelAdmin)
