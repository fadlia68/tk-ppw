from django.shortcuts import render, redirect
from .forms import TaskForm
from .models import Task

# Create your views here.

def index(request):
    if request.user.is_authenticated:
        all_task = request.user.task.order_by('date', 'time', 'title')
    else :
        all_task = []
    return render(request, 'main/todo.html', {'allTask' : all_task})

def addTask(request):
    taskform = TaskForm()
    if request.method == "POST":
        taskForm = TaskForm(request.POST)
        if taskForm.is_valid():
            task = Task(
                title = request.POST['title'],
                description = request.POST['description'],
                category = request.POST['category'],
                date = request.POST['date'],
                time = request.POST['time'],
            )
            task.save()
            request.user.task.add(task)
            return redirect('/todo/')

    return render(request, 'main/add-task.html', {'taskform' : taskform})

def personalTask(request):
    all_task = request.user.task.filter(category = "Personal Task").order_by('date', 'time', 'title')
    return render(request, 'main/personal-task.html', {'allTask' : all_task})

def groupTask(request):
    all_task = request.user.task.filter(category = "Group Task").order_by('date', 'time', 'title')
    return render(request, 'main/group-task.html', {'allTask' : all_task})

def deleteTask(request, task_id):
    task = Task.objects.get(id=task_id)
    task.delete()
    return redirect('/todo/')