from django.db import models

class Pesan(models.Model):
    name = models.CharField(max_length=50, default='Anonymous')
    awal = models.CharField(max_length=1, default='')
    message = models.TextField(max_length=200, default='')
