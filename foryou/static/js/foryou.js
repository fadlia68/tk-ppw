var likes = [];
var x = document.getElementsByClassName("mySlides1");
var y = document.getElementsByClassName("myCount");
var z = document.getElementsByClassName("fa fa-heart");
var i;

for (i = 0; i < x.length; i++) {
    z[i].style.display = "none";
}

showSlides(0)

for (var i = 0; i < x.length; i++) {
    likes[i] = 0;
}

function plusSlides() {
    var o = Math.floor(Math.random() * x.length);
    showSlides(o);
}

function showSlides(n) {
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none"; 
        y[i].style.display = "none"; 
    }
    x[n].style.display = "block";
    y[n].style.display = "block";
}

function updateClickCount(a) {
    var di = Number(a)
    di -= 1;    
    likes[di]++;

    for (i = 0; i < y.length; i++) {
        document.getElementsByClassName("myCount")[i].innerHTML = likes[i];
    }
}

function toggle(a) {
    var di = Number(a)
    di -= 1;    
    document.getElementsByClassName("fa fa-heart-o")[di].style.display = "none";
    document.getElementsByClassName("fa fa-heart")[di].style.display = "block";
} 
