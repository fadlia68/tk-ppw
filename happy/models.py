from django.db import models

# Create your models here.

class Article(models.Model):
    name = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    foto = models.CharField(max_length=100, null=True)
    keyword = models.TextField()

    def __str__(self):
        return self.name

class Comment(models.Model):
    author = models.CharField(max_length=100)
    content = models.TextField()
    email = models.EmailField()

    def __str__(self):
        return self.author

class CommentKdrama(models.Model):
    author = models.CharField(max_length=100)
    content = models.TextField()
    email = models.EmailField()

    def __str__(self):
        return self.author

class CommentAnime(models.Model):
    author = models.CharField(max_length=100)
    content = models.TextField()
    email = models.EmailField()

    def __str__(self):
        return self.author

class CommentRomantic(models.Model):
    author = models.CharField(max_length=100)
    content = models.TextField()
    email = models.EmailField()

    def __str__(self):
        return self.author

class CommentKpop(models.Model):
    author = models.CharField(max_length=100)
    content = models.TextField()
    email = models.EmailField()

    def __str__(self):
        return self.author

class CommentPodcast(models.Model):
    author = models.CharField(max_length=100)
    content = models.TextField()
    email = models.EmailField()

    def __str__(self):
        return self.author

class CommentTiktok(models.Model):
    author = models.CharField(max_length=100)
    content = models.TextField()
    email = models.EmailField()

    def __str__(self):
        return self.author

class CommentGames1(models.Model):
    author = models.CharField(max_length=100)
    content = models.TextField()
    email = models.EmailField()

    def __str__(self):
        return self.author

class CommentGames2(models.Model):
    author = models.CharField(max_length=100)
    content = models.TextField()
    email = models.EmailField()

    def __str__(self):
        return self.author



