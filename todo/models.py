from django.db import models
from django.contrib.auth.models import User

task_categories = (
    ("Personal Task", "Personal Task"),
    ("Group Task", "Group Task"),
)

# Create your models here.
class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="task", null=True)
    title = models.CharField('Task Title', max_length = 100, null = True)
    description = models.TextField('Task Description', null = True)
    category = models.CharField('Task Category', choices = task_categories, default = "Personal Task", max_length=20, null = True)
    date = models.DateField('Date', null = True)
    time = models.TimeField('time', null = True)