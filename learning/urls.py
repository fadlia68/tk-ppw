from django.urls import include, path
from . import views

app_name = 'learning'

urlpatterns = [
    path('', views.index, name='index'),
    path('addSource/', views.addSource, name='addSource'),
    path('programming/', views.programming, name='programming'),
    path('math/', views.math, name='math'),
    path('science/', views.science, name='science'),
    path('language/', views.language, name='language'),
    path('business/', views.business, name='business'),
    path('others/', views.others, name='others'),
    path('allSource/', views.allSource, name='allSource'),
]
