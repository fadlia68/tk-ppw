from django.shortcuts import render, redirect
from .models import *
from .forms import *
import random

# Create your views here.

def randomize():
    subject = set(Subject.objects.all())
    lst = random.sample(subject, 3)
    return lst

def index(request):
    subject = randomize()
    context = {'subject':subject}
    return render(request, 'main/learning.html', context)

def addSource(request):
    form = SubjectForm()
    if request.method == 'POST':
        form = SubjectForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/learning')
    context = {'form' : form}
    return render(request, 'main/addSource.html', context)

def divide(subject):
    lst = []
    temp = []
    for i in range(len(subject)):
        if i%3==0 and i>0:
            lst.append(temp)
            temp = []
        temp.append(subject[i])
    lst.append(temp)
    return lst

def programming(request):
    category = Category.objects.get(name='programming')
    subject = category.subjects.all()
    lst = divide(subject)
    context = {'subject' : subject, 'query' : 'Programming', 'lst':lst}
    return render(request, 'main/more.html', context)

def math(request):
    category = Category.objects.get(name='math')
    subject = category.subjects.all()
    lst = divide(subject)
    context = {'subject' : subject, 'query' : 'Math', 'lst':lst}
    return render(request, 'main/more.html', context)

def science(request):
    category = Category.objects.get(name='science')
    subject = category.subjects.all()
    lst = divide(subject)
    context = {'subject' : subject, 'query' : 'Science', 'lst':lst}
    return render(request, 'main/more.html', context)

def language(request):
    category = Category.objects.get(name='language')
    subject = category.subjects.all()
    lst = divide(subject)
    context = {'subject' : subject, 'query' : 'Language', 'lst':lst}
    return render(request, 'main/more.html', context)

def business(request):
    category = Category.objects.get(name='business')
    subject = category.subjects.all()
    lst = divide(subject)
    context = {'subject' : subject, 'query' : 'Business', 'lst':lst}
    return render(request, 'main/more.html', context)

def others(request):
    category = Category.objects.get(name='others')
    subject = category.subjects.all()
    lst = divide(subject)
    context = {'subject' : subject, 'query' : 'Others', 'lst':lst}
    return render(request, 'main/more.html', context)

def allSource(request):
    subject = Subject.objects.all()
    lst = divide(subject)
    context = {'subject' : subject, 'query' : 'All Sources', 'lst':lst}
    return render(request, 'main/more.html', context)