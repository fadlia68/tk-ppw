from django.test import TestCase, Client
from django.urls import resolve
from .views import index, read, send
from .models import Pesan
from .forms import Input_Form

class ForyouUnitTest(TestCase):

    def test_main_page_url_is_exist_and_using_index_func(self):
        response = Client().get('/foryou/')
        found = resolve('/foryou/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, index)

    def test_read_page_url_is_exist_and_using_read_func(self):
        response = Client().get('/foryou/read/')
        found = resolve('/foryou/read/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, read)

    def test_send_page_url_is_exist_and_using_send_func(self):
        response = Client().get('/foryou/send/')
        found = resolve('/foryou/send/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, send)

    def test_create_model(self):
        new = Pesan.objects.create(name='Aa', message='main kuy', awal='A')
        jumlah = Pesan.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_form_get_method(self):
        response = Client().get('/foryou/send/save')
        form = Input_Form(response)
        self.assertEqual(response.status_code, 302)

    def test_form_post_method_anonymous(self):
        response = Client().post('/foryou/send/save', {'name' : '', 'message' : 'hai'})
        jumlah = Pesan.objects.all().count()
        m = Pesan.objects.get(id=1)
        self.assertEqual(m.name.split()[0], 'Anonymous')
        self.assertEqual(m.message, 'hai')
        self.assertEqual(m.awal, m.name.split()[1][0])
        self.assertEqual(jumlah, 1)
        self.assertEqual(response.status_code, 200)

    def test_form_post_method_person(self):
        response = Client().post('/foryou/send/save', {'name' : 'orang', 'message' : 'hai'})
        jumlah = Pesan.objects.all().count()
        m = Pesan.objects.get(id=1)
        self.assertEqual(m.name, 'orang')
        self.assertEqual(m.message, 'hai')
        self.assertEqual(m.awal, 'O')        
        self.assertEqual(jumlah, 1)
        self.assertEqual(response.status_code, 200)
